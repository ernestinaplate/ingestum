# Changelog

## [unreleased]

- Added support for multiple source locations.
- Added support for multiple artifacts destinations.
- Added support for google datalake.
- Added support for publications as a document type.
- Added support for litcovid as a source.
- Added support for biorxiv as a source.
- Added support for medrxiv as a source.
- Added support for EuropePMC as a source.
- Added support for multiple plugins directories.
- Added support for multithreaded processing.
- Added table extraction transformers based on text markers.
- Added hybrid PDF ingestion.
- Added hybrid PDF tables extraction.
- Added dynamic argument parsing to ingestum-pipeline tool.
- Added from_date and to_date arguments to all literature monitoring transformers.
- Added ingestum-generate-manifest tool.
- Fixed CSV parsing issues.
- Fixed non-ascii characters in output documents.
- Fixed sub-classing document types in plugins.
- Fixed pubmed transformers to handle missing hours attribute.
- Changed camelot-py version to 0.10.1.
- Changed pipeline names from 'excel' to 'xls'.
- Changed plugins folder structure to simplify plugin manager.
- Changed base operating system to Ubuntu 20.04 LTS.
- Removed CSV document type and related transformers.

## [1.3.0] - 2021-07-01

- Added a new "layout" argument to PDFSourceToTextDocument* transformers.
- Added Reddis source and transformer.
- Added "origin" attribute to all document types.
- Added support for recursive conditionals.
- Added tool to merge collection documents.
- Added support for Docker as a development environment.
- Added more details to the API documentation.
- Fixed transformers, documents and conditionals sub-classing.
- Fixed missing mimetype for .xlsx files.
- Fixed import attempts for unnecessary files in plugin directories.
- Fixed loading and deserializing manifest sources plugins.
- Fixed issue with pipeline messing with transformers when used multiple times.
- Changed logging format to JSON.
- Changed tests to pytest.
- Changed PubmedSourceCreate* transformer to use the official entrezpy library.
- Changed PubmedSourceCreate* "hours" argument to be optional.
- Changed PubmedSourceCreate* to use EDAT dates by default.
- Changed to distro-packaged LibreOffice installation.
- Changed artifacts IDs to be randomized.

## [1.2.1] - 2021-03-29

- Fixed extracting sources mimetypes.
- Fixed OR vs AND syntax in PubMed queries.
- Fixed deserializing pipelines with recursive transformers.

## [1.2.0] - 2021-03-19

- Added support for converting HTML to Image source.
- Added support for converting XLS to Image source.
- Added support for converting DOCX to Image source.
- Added support for unspecified number of pages in PDF transformers.
- Added support for recursive transformers, e.g. for collections of collections.
- Added support for context metadata in all document formats.
- Added support for types field in Passage document metadata.
- Added support for toolbox containers as development environment.
- Added ingestum-migrate for existing ingestum documents, e.g for testing outputs.
- Added debug logging calls to all transformers.
- Added debug logging calls to all time critical pipeline steps.
- Fixed opening the same PDF repeatedly for metadata.
- Fixed handling empty PDF pages.
- Fixed source downloading cache.
- Changed to a richer PubMed API.

## [1.1.0] - 2021-02-11

- Added support for DOCX sources.
- Added support for PubMed sources.
- Added support for crop area in PDF source transformer.
- Added support for table extraction from images.
- Added support for PDF unstructured forms.
- Added option to disable PDF columns layot detection.
- Added more examples to documentation.
- Added filters to XML source transformer to reduce noise in output text.
- Fixed PDF column extraction for more complex layouts.
- Fixed error on Text document tokenizer not handling empty lists.
- Fixed error message for non-existent files.
- Fixed errors while ingesting PDF with protections enabled.
- Fixed errors while ingesting PDF with watermarks.
- Fixed errors while ingesting PDF with noisy shape data.
- Fixed errors with HTML parser by switching to lxml.
- Fixed memory consumption in PDF OCR transformers.
- Updated documentation.
- Updated pyexcel requirement to v0.6.6.

## [1.0.2] - 2020-12-10

- Fixed paragraph detection for PDF and OCR pipelines.
- Fixed dealing with PDF images extraction with noisy data.

## [1.0.1] - 2020-11-30

- Changed version of requests to 2.24.0.

## [1.0.0] - 2020-11-30

- Initial release.
